import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './redux/rootReducer';
import rootSaga from './redux/rootSaga';
import './index.css';
import App from './App';
import { StateProvider } from './redux/ContextProvider';
// import reportWebVitals from './reportWebVitals';
// import { ContextProvider } from './contexts/ContextProvider';

// Create the Redux Saga middleware
const sagaMiddleware = createSagaMiddleware();

// Create the Redux store with middleware
const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
);

// Run the rootSaga to initialize sagas
sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <StateProvider>
        <App />
      </StateProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);