import { combineReducers } from 'redux';
import userReducer from './user/userSlice';

const rootReducer = combineReducers({
  user: userReducer,
  // Add more reducers as needed
});

export default rootReducer;
