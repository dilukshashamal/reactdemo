import React from "react";
import { Grid } from "@mui/material";
import Tile from "../../components/Dashboard/Tile";

const moduleData = [
  {
    id: "1",
    name: "View Students",
    image:
      "https://img.freepik.com/free-photo/blackboard-inscribed-with-scientific-formulas-calculations_1150-19413.jpg?size=626&ext=jpg&ga=GA1.1.1599894178.1703658316&semt=sph",
    description: "Description for Module 1",
    navLink: "/admin/student",
  },

  {
    id: "2",
    name: "View Lecturers",
    image:
      "https://img.freepik.com/free-photo/blackboard-inscribed-with-scientific-formulas-calculations_1150-19413.jpg?size=626&ext=jpg&ga=GA1.1.1599894178.1703658316&semt=sph",
    description: "Description for Module 1",
    navLink: "/admin/lecturer",
  },

  {
    id: "3",
    name: "View Modules",
    image:
      "https://img.freepik.com/free-photo/blackboard-inscribed-with-scientific-formulas-calculations_1150-19413.jpg?size=626&ext=jpg&ga=GA1.1.1599894178.1703658316&semt=sph",
    description: "Description for Module 1",
    navLink: "/admin/module",
  },

  // Add more modules as needed
];
const AdminDash = () => {
  return (
    <Grid container spacing={2} justifyContent="center" alignItems="center">
      {moduleData.map((subject) => (
        <Grid item key={subject.id} xs={12} sm={6} md={4} lg={3}>
          <Tile
            title={subject.name}
            image={subject.image}
            description={subject.description}
            linkTo={subject.navLink}
          />
        </Grid>
      ))}
    </Grid>
  );
};

export default AdminDash;
